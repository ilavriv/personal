from flask import render_template, request, session, redirect, url_for

from app.settings import ApplicationDefines

from . import account
from forms import LoginForm


@account.route('/login/', methods=('GET', 'POST'))
def login():
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        username = form.data['username']
        password = form.data['password']
        admin = ApplicationDefines.ADMIN
        if username == admin['username'] and password == admin['password']:
            session.update({
                'username': username
            })
            return redirect(url_for('admin.posts'))
    context = {'form': form}
    return render_template('account/login.html', **context)


@account.route('/logout/')
def logout():
    del session['username']
    return redirect(url_for('account.login'))
