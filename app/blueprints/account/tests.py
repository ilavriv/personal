import unittest

from app import app, db, settings


class AccounTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        with app.app_context():
            app.config.from_object(settings.TestSettings)
            db.create_all()

    def _login(self):
        self.app.post(
            '/account/login/',
            data=dict(
                username='test_user',
                password='test_password'
            ),
            follow_redirects=True
        )

    def tearDown(self):
        del self.app
        db.session.rollback()
        db.drop_all()

    def test_login(self):
        data = {
            'username': 'test_user',
            'password': 'test_password'
        }
        self.app.post(
            '/account/login/',
            data=data,
            follow_redirects=True
        )
        with self.app.session_transaction() as s:
            username = s.pop('username', None)
            self.assertIsNotNone(username)

    def test_logout(self):
        self._login()
        self.app.get('/account/logout/')
        with self.app.session_transaction() as s:
            username = s.pop('username', None)
            self.assertIsNone(username)
