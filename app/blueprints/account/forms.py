from flask_wtf import Form
from wtforms import StringField, PasswordField
from wtforms.validators import Required


class LoginForm(Form):
    username = StringField(u'username', validators=[Required()])
    password = PasswordField(u'password', validators=[Required()])
