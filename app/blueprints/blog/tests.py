import unittest
from app import app, db, settings

from .models import Post


class BlogSimpleTestCase(unittest.TestCase):

    def setUp(self):
        with app.app_context():
            app.config.from_object(settings.TestSettings)
            db.create_all()

    def test_create_post(self):
        p = Post(
            title='test',
            slug='test-post',
            content='test content'
        )
        db.session.add(p)
        db.session.commit()
        posts = Post.query.all()
        len_posts = len(posts)
        self.assertEqual(1, len_posts)

    def tearDown(self):
        db.session.remove()
        db.drop_all()
