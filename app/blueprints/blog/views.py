from flask import render_template, request, abort

from app import db
from . import blog
from .models import Post

from app.utils import DefaultOrderedDict
from app.settings import ApplicationDefines


@blog.route('/posts/')
def posts():
    page = request.view_args.get('page', 1)
    query = Post.query.filter_by(
        _is_published=True
    ).order_by(Post.published_date.desc()).paginate(
        page, ApplicationDefines.PAGINATE_BY)
    context = {'query': query}
    return render_template('blog/posts.html', **context)


@blog.route('/posts/<post_slug>/')
def post_details(post_slug):
    post = db.session.query(Post).filter_by(slug=post_slug).first()
    if post is None and not post.is_published:
        abort(404)
    context = {'post': post}
    return render_template('blog/post_details.html', **context)


@blog.route('/archive/')
def archive():
    payload = DefaultOrderedDict(list)
    query = db.session.query(Post).filter_by(
        _is_published=True
    ).order_by(Post.published_date.desc())
    for p in query:
        pmonth = p.published_date.month
        pyear = p.published_date.year
        if pmonth not in payload[pyear]:
            payload[pyear].append(pmonth)
    for k, v in payload.iteritems():
        payload[k] = sorted(v, reverse=True)
    context = {'dates': payload}
    return render_template('/blog/dates_list.html', **context)


@blog.route('/archive/<int:year>/<int:month>/<int:day>/')
@blog.route('/archive/<int:year>/<int:month>/')
@blog.route('/archive/<int:year>/')
def archive_posts(year=None, month=None, day=None):
    page = request.view_args.get('page', 1)
    query = Post.query.filter_by(_is_published=True)
    from .utils import get_range_date
    from_date, to_date = get_range_date(year, month, day)
    if from_date and to_date:
        query = query.filter(
            Post.published_date >= from_date, Post.published_date < to_date
        ).order_by(Post.published_date.desc())
    query = query.paginate(page, ApplicationDefines.PAGINATE_BY)
    context = {'query': query}
    return render_template('blog/posts.html', **context)
