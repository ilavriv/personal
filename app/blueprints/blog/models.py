from datetime import datetime

from app import db


class Post(db.Model):
    __tablename__ = u'posts'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100))
    slug = db.Column(db.String(100), unique=True)
    _is_published = db.Column(db.Boolean, default=False)
    published_date = db.Column(db.DateTime)
    content = db.Column(db.Text)

    @property
    def is_published(self):
        return self._is_published

    @is_published.setter
    def is_published(self, value):
        if value:
            self.published_date = datetime.now()
        self._is_published = value

    def __repr__(self):
        return u'<Post {0}>'.format(self.slug)
