from datetime import datetime


def get_range_date(year=None, month=None, day=None):
    from_date = None
    to_date = None
    if year and month and day:
        from_date = datetime(year, month, day)
        to_date = from_date
    elif year and month:
        from_date = datetime(year, month, 1)
        month += 1
        if month > 12:
            month = 1
            year += 1
        to_date = datetime(year, month, 1)
    elif year:
        from_date = datetime(year, 1, 1)
        to_date = datetime(year + 1, 1, 1)
    return from_date, to_date
