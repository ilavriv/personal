from flask_wtf import Form
from wtforms import (
    StringField, BooleanField, TextAreaField
)
from wtforms.validators import DataRequired


class PostForm(Form):
    title = StringField(u'title', validators=[DataRequired()])
    slug = StringField(u'slug', validators=[DataRequired()])
    content = TextAreaField(u'content', validators=[DataRequired()])
    is_published = BooleanField(u'is published')
