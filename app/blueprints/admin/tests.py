import unittest

from app import app, db, settings
from app.blueprints.blog.models import Post


class AdminTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()
        with app.app_context():
            app.config.from_object(settings.TestSettings)
            db.create_all()

    def tearDown(self):
        del self.app
        db.session.rollback()
        db.drop_all()

    def test_create_form(self):
        data = {
            'title': 'test post',
            'content': 'test post content',
            'slug': 'test-post'
        }
        self.app.post(
            '/admin/posts/new/',
            data=data,
            follow_redirects=True
        )
        posts_count = Post.query.filter_by(title='test post').count()
        self.assertEqual(1, posts_count)

    def test_update_post(self):
        p = Post(
            title='old title',
            content='old content',
            slug='old-slug'
        )
        db.session.add(p)
        db.session.commit()
        data = {
            'title': 'new title',
            'content': 'new content',
            'slug': 'new-slug'
        }
        self.app.post(
            '/admin/posts/1/update/',
            data=data,
            follow_redirects=True
        )
        post = Post.query.first()
        self.assertEqual(post.title, 'new title')
        self.assertEqual(post.slug, 'new-slug')
        self.assertEqual(post.content, 'new content')

    def test_delete_post(self):
        p = Post(
            title='for delete',
            content='content for delete',
            slug='slug-for-delete'
        )
        db.session.add(p)
        db.session.commit()
        self.app.get(
            '/admin/posts/1/delete',
            follow_redirects=True
        )
        post = Post.query.filter_by(
            title='for delete'
        ).first()
        self.assertIsNone(post)
