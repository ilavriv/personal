from flask import session, render_template, redirect, url_for, abort
from app import db
from app.blueprints.blog.models import Post

from app.settings import ApplicationDefines

from . import admin
from .forms import PostForm


@admin.before_request
def before_admin():
    username = session.get('username', None)
    admin = ApplicationDefines.ADMIN.get('username')
    if username is None or username != admin:
        return redirect(url_for('account.login'))


@admin.route('/posts/')
def posts():
    posts = Post.query.all()
    context = {'posts': posts}
    return render_template('admin/posts.html', **context)


@admin.route('/posts/new/', methods=('GET', 'POST'))
def new_post():
    form = PostForm()
    context = {'form': form}
    if form.validate_on_submit():
        post = Post(**form.data)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('admin.posts'))
    return render_template('admin/new_post.html', **context)


@admin.route('/posts/<int:post_id>/')
def post_details(post_id):
    post = db.session.query(Post).get(post_id)
    if post is None:
        abort(404)
    context = {'post': post}
    return render_template('admin/post_details.html', **context)


@admin.route('/posts/<int:post_id>/update/', methods=('GET', 'POST'))
def update_post(post_id):
    post = db.session.query(Post).get(post_id)
    if post is None:
        abort(404)
    form = PostForm()
    if form.validate_on_submit():
        form.populate_obj(post)
        db.session.add(post)
        db.session.commit()
        return redirect(url_for('admin.posts'))
    context = {
        'form': form,
        'post': post
    }
    return render_template('admin/update_post.html', **context)


@admin.route('/posts/<int:post_id>/delete/')
def delete_post(post_id):
    post = db.session.query(Post).get(post_id)
    if post is None:
        abort(404)
    db.session.delete(post)
    db.session.commit()
    return redirect(url_for('admin.posts'))
