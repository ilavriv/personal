from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy

from . import settings


app = Flask(__name__)
app.config.from_object(settings.BaseSettings)
db = SQLAlchemy(app)


@app.template_filter('month_name')
def get_month_name(month_number):
    from calendar import month_name
    return month_name[month_number]


def bp_register():
    from blueprints.core import core
    from blueprints.account import account
    from blueprints.blog import blog
    from blueprints.admin import admin

    app.register_blueprint(core, url_prefix='/')
    app.register_blueprint(account, url_prefix='/account')
    app.register_blueprint(blog, url_prefix='/blog')
    app.register_blueprint(admin, url_prefix='/admin')

bp_register()
db.create_all()
