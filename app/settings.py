class BaseSettings(object):
    SECRET_KEY = '213213424324fdsafdsafd'
    SQLALCHEMY_DATABASE_URI = 'mysql://root:password@localhost/personal'


class ApplicationDefines(object):
    PAGINATE_BY = 10
    ADMIN = {
        'username': u'root',
        'password': u'password'
    }


class TestSettings(BaseSettings):
    TESTING = True
    CSRF_ENABLED = False
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = 'mysql://root:password@localhost/test_personal'
